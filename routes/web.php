<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/books/upload','UploadController@index')->name('upload.file');
Route::post('/books/import','UploadController@importBookList')->name('import.file');

Route::get('/books', 'BookController@index')->name('books');
Route::get('/books/create', 'BookController@create')->name('books.create');
Route::get('/books/{id}', 'BookController@show')->name('books.show');
Route::post('/books', 'BookController@store')->name('books.store');
Auth::routes();
Route::get('/books/edit/{id}','BookController@edit')->name('books.edit');
Route::put('/books/{id}','BookController@update')->name('books.update');

Route::get('/home', 'HomeController@index')->name('home');

