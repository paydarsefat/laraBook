<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'پسورد شما باید بیشتر از 6 کاراکتر باشد',
    'reset' => 'پسورد شما ریست شد',
    'sent' => 'ما به شماره شما کد فراموشی رمز را ارسال کردیم!',
    'token' => 'این کد فراموشی رمز معتبر نیست',
    'user' => "ما نتوانستیم کد را به شماره شما ارسال کنیم",

];
