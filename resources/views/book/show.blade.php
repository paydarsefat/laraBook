@extends('layouts.app')
@section('content')
    <div class="container">
    {{--@foreach($books as $book)--}}
        <h1  class="mt-5">
            {{"Name: ".$books->name }}
        </h1>
        <h3  class="mt-5">
            {{"ISBN: ".$books->ISBN }}
        </h3>
        <h3  class="mt-5">
            {{ "Pages :".$books->pages }}
        </h3>
        <h3  class="mt-5">
            {{ "Published: ".$books->published_at }}
        </h3>
        <h3  class="mt-5">
            {{"Price: ".$books->price }}
        </h3>
        <h3  class="mt-5">
            {{"Creator: ".$books->user->name }}
        </h3>
        <p>Category :</p>
            <div class="container">
            @foreach($books->categories as $category)
                        <em>{{$category->name.', ' }}</em>
                @endforeach
            </div>
        <p>Author :</p>
        <div class="container">
            @foreach($books->authors as $author)
                <em>{{$author->name.', ' }}</em>
            @endforeach
        </div>
    </div>
@endsection