@extends('layouts.app')
@section('content')
    <div class="container col-lg-offset-2">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{route('import.file')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Example file input</label>
                        <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </form>
            </div>
         <div>
    <div>
@endsection
