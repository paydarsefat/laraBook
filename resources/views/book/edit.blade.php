@extends('layouts.app')

@section('content')
    <div class="container col-lg-offset-2">
        <div class="card">
            <div class="card-body">
                <form class="form-signin" method="POST" action="{{route('books.update', ['id' => $book->id])}}">
                    {{ csrf_field()}}
                    {{ method_field('PUT') }}
                    <div class="text-center mb-4">
                        <h1 class="h3 mb-3 font-weight-normal">Create Books</h1>
                    </div>
                    <div class="form-label-group">
                        <br>
                        <label for="inputeName">Enter Book Name</label>
                        <input type="name" name="name"  value="{{$book->name}}" id="inputeName" class="form-control"  >
                    </div>

                    <div class="form-label-group">
                        <br>
                        <label for="inputPages">Pages</label>
                        <input type="text" name="pages" value="{{$book->pages}}" id="inputPages" class="form-control"  >
                    </div>
                    <div class="form-label-group">
                        <br>
                        <label for="inputPassword">ISBN</label>
                        <input type="text" name="ISBN" value="{{$book->ISBN}}" id="inputISBN" class="form-control"  >
                    </div>
                    <div class="form-label-group">
                        <br>
                        <label for="inputPrice">Price</label>
                        <input type="text" name="price" value="{{$book->price}}" id="inputPrice" class="form-control" >

                    </div>
                    <div class="form-label-group">
                        <br>
                        <label for="inputPublished">Published_at</label>
                        <input type="text" name="published_at" value="{{$book->published_at}}" id="inputPublished" class="form-control"  >
                    </div>
                    <div class="form-label-group" >
                        <label>Categories</label>
                        <select name="category_id[]" class="form-control" id="exampleFormControlSelect2" multiple >
                            @foreach($categories as $category)
                                <option value={{$category->id}} {{$book->categories->contains('id', $category->id) ? 'selected' : ''}}>
                                    {{$category->name.","}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-label-group" >
                        <label>Author</label>
                        <select name="author_id[]" class="form-control" id="exampleFormControlSelect2" multiple >
                            @foreach($authors as $author)
                                <option value={{$author->id}} {{$book->authors->contains('id', $author->id) ? 'selected' : ''}}>
                                    {{$author->name.","}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn-center btn btn-lg btn-primary  btn-primary"  type="Submit">Update</button>
                    </div>
                </form>
                @include('book.errors')
            </div>
        </div>
    </div>

@endsection