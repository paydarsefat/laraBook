@extends('layouts.app')

@section('content')
    <div class="container col-6">
        <table class="table">
            <tr>
                <th>
                    Book Name
                </th>
                <th>
                    Creator
                </th>
                <th>
                    Author
                </th>
                <th></th>
            </tr>
            @foreach($books as $book)
                <tr>
                    <td><a class="mt-5" href="{{"/books/".$book->id}}">{{$book->name}}</a></td>
                    <td>{{$book->user->name}}</td>
                    @foreach($book->authors as $author)
                       <td> {{$author->name}} </td>
                    @endforeach
                    @can('update',$book)
                    <td><a class="mt-5" href="{{route("books.edit", ['id'=>$book->id])}}" class="btn btn-primary">Update</a></td>
                        @else
                        <td></td>
                    @endcan
                </tr>
            @endforeach

        </table>
        <div class="mt-3"></div>
        <a href="{{route('books.create')}}" type="button" class="btn  col-6 btn-success  btn-block " >Create</a>
    </div>
@endsection