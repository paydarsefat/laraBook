@extends('layouts.app')

@section('content')
    <div class="container col-lg-offset-2">
       <div class="card">
           <div class="card-body">
               <form class="form-signin" method="POST" action="{{route('books.store')}}">
                   {{ csrf_field() }}
                   <div class="text-center mb-4">
                       <h1 class="h3 mb-3 font-weight-normal">Create Books</h1>
                   </div>
                   <div class="form-label-group">
                       <br>
                       <label for="inputeName">Enter Book Name</label>
                       <input type="name" name="name"  value="{{old('name')}}" id="inputeName" class="form-control"  >

                   </div>

                   <div class="form-label-group">
                       <br>
                       <label for="inputPages">Pages</label>
                       <input type="text" name="pages" value="{{old('pages')}}" id="inputPages" class="form-control"  >
                   </div>
                   <div class="form-label-group">
                       <br>
                       <label for="inputPassword">ISBN</label>
                       <input type="text" name="ISBN" value="{{old('ISBN')}}" id="inputISBN" class="form-control"  >
                   </div>
                   <div class="form-label-group">
                       <br>
                       <label for="inputPrice">Price</label>
                       <input type="text" name="price" value="{{old('price')}}" id="inputPrice" class="form-control" >

                   </div>
                   <div class="form-label-group">
                       <br>
                       <label for="inputPublished">Published_at</label>
                       <input type="text" name="published_at" value="{{old('published_at')}}" id="inputPublished" class="form-control"  >
                   </div>
                   <div class="form-label-group" >
                       <label>Categories</label>
                       <select name="category_id[]" class="form-control" id="exampleFormControlSelect2" multiple >
                           @foreach($categories as $category)
                              <option value={{$category->id}}>{{$category->name.","}}</option>
                           @endforeach
                       </select>
                   </div>
                   <div class="form-label-group" >
                       <label>Author</label>
                       <select name="author_id[]" class="form-control" id="exampleFormControlSelect2" multiple >
                           @foreach($authors as $author)
                               <option value={{$author->id}}>{{$author->name.","}}</option>
                           @endforeach
                       </select>
                   </div>
                   <div class="text-center">
                        <button class="btn-center btn btn-lg btn-primary  btn-success"  type="Submit">Submit</button>
                   </div>
               </form>
               @include('book.errors')
           </div>
       </div>
    </div>

 @endsection