<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /*          \App\Book::create(['name'=>'bookone','pages'=>'200','ISBN'=>'123456','price'=>'300','published_at'=>'1394-10-11']);
                \App\Book::create(['name'=>'booktwo','pages'=>'210','ISBN'=>'123457','price'=>'300','published_at'=>'1396-10-11']);
                \App\Book::create(['name'=>'booktree','pages'=>'210','ISBN'=>'123458','price'=>'300','published_at'=>'1396-10-11']);
                \App\Book::create(['name'=>'bookfour','pages'=>'210','ISBN'=>'123460','price'=>'300','published_at'=>'1396-10-11']);
                \App\Book::create(['name'=>'bookfive','pages'=>'310','ISBN'=>'123460-D','price'=>'300','published_at'=>'1396-10-11']);
    */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->unsignedInteger('pages');
            $table->string('ISBN')->unique();
            $table->integer('price');
            $table->date('published_at');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }
/*          \App\Book::create(['name'=>'bookone','pages'=>'200','ISBN'=>'123456','price'=>'300','published_at'=>'1394-10-11']);
            \App\Book::create(['name'=>'booktwo','pages'=>'210','ISBN'=>'123457','price'=>'300','published_at'=>'1396-10-11']);
            \App\Book::create(['name'=>'booktree','pages'=>'210','ISBN'=>'123458','price'=>'300','published_at'=>'1396-10-11']);
            \App\Book::create(['name'=>'bookfour','pages'=>'210','ISBN'=>'123460','price'=>'300','published_at'=>'1396-10-11']);
            \App\Book::create(['name'=>'bookfive','pages'=>'310','ISBN'=>'123460-D','price'=>'300','published_at'=>'1396-10-11']);
*/
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
