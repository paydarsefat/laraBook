<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\User::class, 10)->create()->each(function ($user){
            $user->books()->create(factory(\App\Book::class)->make()->toArray())
                 ->each(function ($book){
                       $book->authors()->attach('1');
                       $book->categories()->attach('2');
                    });
        });

    }
}
