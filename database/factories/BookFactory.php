<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name ,
        'pages'=> $faker->randomNumber(4),
        'ISBN' => $faker->numberBetween($min = 1000000000, $max = 9000000000),
        'price'=> $faker->randomNumber(3),
        'published_at' => $faker->date()
    ];
});
