<?php

namespace App\Listeners;

use App\Events\CreateBook;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Kavenegar\KavenegarApi;

class SendCreateBookSms implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateBook  $event
     * @return void
     */
    public function handle(CreateBook $event)
    {
        //
        $bookName = $event->book['name'];
        $userName = $event->user->name;
        $msg = $userName." ممنون از ثبت کتاب : ".$bookName;
        $client = new KavenegarApi(env('KAVEH_NEGAR_API_KEY'));
        $client->Send(env('SENDER_MOBILE'), env('RECEIVER_MOBILE'), $msg);

    }
}
