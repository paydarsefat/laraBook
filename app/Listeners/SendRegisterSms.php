<?php

namespace App\Listeners;

use App\Events\RegisterUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Kavenegar\KavenegarApi;

class SendRegisterSms implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterUser  $event
     * @return void
     */
    public function handle(RegisterUser $event)
    {
        //
        $userName = $event->user['name'];
        $msg = " $userName به سایت ما خوش آمدید . ";
        $client = new KavenegarApi(env('KAVEH_NEGAR_API_KEY'));
        $client->Send(env('SENDER_MOBILE'), env('RECEIVER_MOBILE'), $msg);
    }
}
