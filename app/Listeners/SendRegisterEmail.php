<?php

namespace App\Listeners;

use App\Events\RegisterUser;
use App\Mail\CreateBookMail;
use App\Mail\RegisterUserMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendRegisterEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterUser  $event
     * @return void
     */
    public function handle(RegisterUser $event)
    {
        //
       $email = $event->user['email'];
       Mail::to($email)->send(new RegisterUserMail($event->user));
    }
}
