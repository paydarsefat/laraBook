<?php

namespace App\Listeners;

use App\Events\CreateBook;
use App\Mail\CreateBookMail;
use App\Mail\CreateBookMaile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendCreateBookEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateBook  $event
     * @return void
     */
    public function handle(CreateBook $event)
    {
        //
        /** @var TYPE_NAME $event */
        Mail::to($event->user)->send(new CreateBookMail($event->book, $event->user));
    }
}
