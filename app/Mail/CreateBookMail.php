<?php

namespace App\Mail;

use App\Book;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateBookMail extends Mailable
{
    use Queueable, SerializesModels;

    public $book;
    public $user;
    public function __construct(Book $book,User $user)
    {
        //
        $this->book = $book;
        $this->user = $user ;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.createbook');
    }
}
