<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|max:250',
            'pages'        => 'required|numeric',
            'ISBN'         => 'required|size:10',
            'price'        => 'required',
            'published_at' => 'required|date',
            'category_id'  => 'required',
            'author_id'    => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'          => 'شما :attribute را وارد نکردید',
            'name.max'               => 'شما :attribute را بیشتر از :max کاراکتر وارد کردید',
            'pages.required'         => 'شما :attribute را وارد نکردید',
            'pages.numeric'          => 'لطفا برای :attribute فقط عدد وارد کنید',
            'ISBN.required'          => 'شما :attribute را وارد نکردید',
            'ISBN.size'              => ':attribute باید :size کاراکتر باشد',
            'price.required'         => 'شما :attribute را وارد نکردید',
            'published_at.required'  => 'شما :attribute را وارد نکردید',
            'published_at.date'      => 'لطفا:attribute را با فرمت درست وارد نمایید به طور مثال 13-12-1394',
            'category_id.required'   => 'شما :attribute را انتخاب نکردید'
        ];
    }
}
