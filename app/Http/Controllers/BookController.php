<?php

namespace App\Http\Controllers;



use App\Author;
use App\Book;
use App\Category;
use App\Events\CreateBook;
use App\Http\Requests\StoreBookRequest;
use App\Listeners\SendCreateBookEmail;
use App\Services\StoreService;
use App\Services\UpdateService;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'update']);
    }

    public function index()
    {
        $books = Book::orderBy('id')->get();
        return view('book.index', compact('books'));
    }

    public function show($id)
    {
        $books = Book::find($id);
        return view('book.show', compact('books'));
    }

    public function create()
    {
        $categories = Category::all();
        $authors = Author::all();
        return view('book.create', compact('categories', 'authors'));
    }

    /**
     * @param StoreBookRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreBookRequest $request)
    {
        $books = new StoreService();
        $books = $books->make($request);

        return redirect()->route('books');
    }

    public function edit($id)
    {
        $book = Book::find($id);

        $this->authorize('update', $book);

        $categories = Category::all();
        $authors = Author::all();

        return view('book.edit', compact('book', 'categories', 'authors'));
    }

    public function update(Request $request, $id)
    {
        $book = new UpdateService();
        $book = $book->update($request, $id);

        return redirect()->route('books.show', ['id' => $book->id]);
    }
}
