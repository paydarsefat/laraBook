<?php

namespace App\Http\Controllers;

use App\Http\ExcelFile\BookListImport;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    //
    public function __construct()
    {
       return $this->middleware('auth')->only(['importBookList']);
    }

    public function index(){
        return view('book.upload');
    }

    public function importBookList(BookListImport $import){
        $A = $import->handleImport();
        dd("s:".$A);

    }
}
