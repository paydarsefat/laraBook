<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 7/10/18
 * Time: 6:22 PM
 */
namespace App\Http\ExcelFile;


use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Files\ExcelFile;

class BookListImport extends ExcelFile
{
    /**
     * @return mixed
     */
    protected $delimiter  = ',';
    protected $enclosure  = '"';
    protected $lineEnding = '\r\n';
    public function getFile()
    {

        // Import a user provided file
        $file = Input::file('file');


        // Return it's location
        return $file;
      // return storage_path('exports') . '/file.csv';
    }

    public function getFilters()
    {
        return [
            'file' => 'required|file'
        ];
    }

}
