<?php

namespace App\Http\ExcelFile;


use App\Author;
use App\Book;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Files\ImportHandler;

class BookListImportHandler implements ImportHandler {

    /**
     * Handle the import
     * @param $file
     * @return mixed
     */
    public function createBooksFromExcel($reader){
        $books = Book::all();
        foreach ($reader->toArray() as $key => $row) {
            //dd($row['isbn']);
            $data['name'] = $row['name'];
            $data['pages'] = $row['page'];
            $data['ISBN'] = $row['isbn'];
            $data['price'] = $row['price'];
            $data['published_at'] = $row['published_at'];
            if (!empty($data)) {
                //DB::table('books')->insert($data);
                $book = Auth::user()->books()->create($data);
                $book->categories()->attach([1]);
                $book->authors()->attach(2);
            }
        }
        dd('finish');
    }
    public function handle($file)
    {
        $path = $file->getFile()->getRealPath();
        Excel::load($path, function ($reader) {
            $this->createBooksFromExcel($reader);
        });

    }

}