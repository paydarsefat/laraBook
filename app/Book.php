<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
       /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */

    protected $fillable = ['name','slug', 'pages','user_id', 'ISBN', 'price','email', 'published_at'];
    //

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function  authors()
    {
        return $this->belongsToMany(Author::class);
    }
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
