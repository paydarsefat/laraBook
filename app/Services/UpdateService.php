<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 7/23/18
 * Time: 7:39 PM
 */

namespace App\Services;


use App\Book;

class UpdateService
{
        public function update($request, $id)
        {
            //find
            $book = Book::find($id);

            //update field
            $book->update($request->only(['name', 'pages', 'ISBN', 'price', 'published_at']));
            //update relation
            $book->categories()->sync($request->get('category_id'));
            $book->authors()->sync($request->get('author_id'));

            return $book;
        }
}