<?php
namespace App\Services;

use App\Events\CreateBook;
use App\Http\Requests\StoreBookRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 7/23/18
 * Time: 6:20 PM
 */

class StoreService
{
        public function make(StoreBookRequest $request)
        {

            $book = Auth::user()->books()->create($request->except(['_token']));
            $book->categories()->attach($request->get('category_id'));
            $book->authors()->attach($request->get('author_id'));

            event(new CreateBook($book, Auth::user()));

            return $book;
        }
}